import 'package:flutter/material.dart';

class Tabs extends StatefulWidget {
  Tabs({Key? key}) : super(key: key);

  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int currentIndex = 0;
  List page = [
    Text('首页'),
    Text('分类'),
    Text('设置'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('案例'),
      ),
      body: Center(
        child: this.page[this.currentIndex],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: this.currentIndex,
        items: [
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: '首页',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.category),
            label: '分类',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: '设置',
          ),
        ],
        onTap: (int index) {
          print(index);
          setState(() {
            this.currentIndex = index;
          });
        },
      ),
    );
    ;
  }
}
